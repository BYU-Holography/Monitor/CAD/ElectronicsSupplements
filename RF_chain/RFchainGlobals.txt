"ampBoardWidth" = .76in
"ampBoardLength" = 1.12in
"BoardThick" = .06in

"filterBoardLength" = 1.3in
"filterBoardWidth" = .76in

"mixerBoardLength" = 1.3in
"mixerBoardWidth" = .76in

"mixerboxLength" = .313in
"mixerboxWidth" = .225in
"mixerboxThick" = .13in
"mixerBoxDistEdge" = .175 'distance to the closest long edge of the board
"mixerBoxDistEnd" = .45 'distance to the closest short end of the board

"Hela10Width" = .76in
"Hela10Length" = 2.33in
"Hela10Thick" = .06in
"Hela10boxDistEnd" = .4in
